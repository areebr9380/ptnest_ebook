<?php session_start();


	include_once ('config.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<?php
			include("includes/head.inc.php");
		?>
</head>

<body>
			<!-- start header -->
				<div id="header">
					<div id="menu">
						<?php
							include("includes/menu.inc.php");
						?>
					</div>
				</div>

				<div id="logo-wrap">
					<div id="logo">
							<?php
								include("includes/logo.inc.php");
							?>
					</div>
				</div>
			<!-- end header -->

			<!-- start page -->

				<div id="page">
					<!-- start content -->
							<div id="content">
								<div class="post">
									<h1 class="title">Album</h1>
									<div class="entry">

										<table width="100%" >
											<br><br><br><br><br>
											<?php

												$query="select *from gallery_album";

												$res=mysql_query($query,$link) or die("Can't Execute Query...");

												$count=0;
												while($row=mysql_fetch_assoc($res))
												{
													if($count==0)
													{
														echo '<tr>';
													}
													echo '<td valign="top" width="20%" align="center">
														<a href="gallery_images.php?id='.$row['id'].'">
														<img src="'.$row['thubnail_image'].'" width="100" height="100">
														<br>'.$row['album_name'].'</a>
													</td>';
													$count++;

													if($count==2)
													{
														echo '</tr>';
														$count=0;
													}
												}
											?>

										</table>
									</div>

								</div>

							</div>
					<!-- end content -->

					<!-- start sidebar -->
							<div id="sidebar">
									<?php
										include("includes/search.inc.php");
									?>
							</div>
					<!-- end sidebar -->
					<div style="clear: both;">&nbsp;</div>
				</div>
			<!-- end page -->


			<!-- start footer -->
				<div id="footer">
							<?php
								include("includes/footer.inc.php");
							?>
				</div>
			<!-- end footer -->
</body>
</html>
